# K plus proches voisins

Le travail proposé consiste à programme l'algorithme des K plus proches voisins, et à l'utiliser sur la base de données d'iris.

Puis, nous utilisons `scikit-learn` pour réaliser la même tâche, sur la base d'iris, ainsi que sur des mesures de
masse observées par imagerie lors de dépistages du cancer du sein. Le travail est disponible sous forme d'un notebook : [notebook_kpp.ipynb](notebook_kpp.ipynb)

