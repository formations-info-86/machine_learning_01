# Arbre de décision

Un arbre de décision est un arbre contenant un test sur chaque nœud interne, qui
permet de classifier ou de faire de la régression. Ici, nous nous plaçons dans
le cas de la classification, avec des échantillons contenant des
caractéristiques numériques continues. Toutefois, les arbres de décision peuvent
être adaptés à des caractéristiques (*features*) discrètes et à de la régression...

La fiche de cours sur le sujet est disponible en ligne : 
[Arbres de décision](https://formations-info-86.gitlab.io/nsi_2022/PDF/C_decision_tree.pdf)

## On code ?

Voici une *proposition* de développement. Faire un module `decision_tree.py` qui implémente les arbres de décision pour les problèmes de classification, et pour des *features* continues (comme dans l'exemple de la fiche de cours). L'arbre produit sera binaire.

Un squelette de code documenté est donné dans le fichier [decision_tree.py](decision_tree.py).  En vous y conformant, il sera possible de vous fournir les fonctions que vous n'aurez pas le temps de réaliser vous-même.

En ce qui concerne la représentation des arbres, nous utilisons simplement des tuples imbriqués. Si vous avez déjà une classe Python
pour représenter des arbres binaires, n'hésitez pas à vous en servir !

Si vous bloquez, demandez le code de la fonction qui vous pose problème.

## Essais sur la base d'Iris

Une fois votre code au point, il sera temps de le tester sur la base d'Iris :

```python
import csv
import random
import numpy as np
import decision_tree # <<= Votre module

##

def test_tree(tree, test: np.array):
    """
    Procédure indiquant la performance de l'arbre de décision
    :param tree: un arbre de décision
    :param test: un ensemble d'échantillons à classer (une matrice)
    :return: un tuple contenant le pourcentage de bien classés et le **nombre** de bien classés
    """
    ...

## ------------ SCRIPT ------------------------------

# 0n utilise des classes numériques
classe = {"Iris-setosa": 0, "Iris-versicolor": 1, "Iris-virginica": 2}

# Pensez à utiliser le script fourni pour télécharger les données
# dans un premier temps
with open('data/iris.csv', mode='r') as iris_file:
    irises = list(csv.reader(iris_file))
    liste = []
    for iris in irises:
        if not iris: continue
        echantillon = [float(n) for n in iris[0:4]]
        echantillon.append(classe[iris[4]])
        liste.append(echantillon)
# affichage de quelques valeurs
for k in range(5):
    print(liste[k])

# Les données sont initialement groupées par classe
# c'est pourquoi il faut les mélanger.
# Ensuite, on utilisera les 100 premières valeurs pour l'apprentissage
# et les 50 valeurs suivantes pour tester.
# Finir en affichant le pourcentage de réussite.
...
```

Les performances attendues sont aux alentours de 94 % d'iris bien classés (sur la base de test).
L'arbre de décision varie selon le mélange initial (`random.shuffle`) 
qui modifie les échantillons utilisés pour l'apprentissage.
Mais assez typiquement, l'arbre obtenu ressemble à celui-ci :

```python
('Node', (2, 2.5), 
         ('Leaf', {0.0: 33}), 
         ('Node', (3, 1.7000000000000002), 
                  ('Node', (2, 4.95), 
                           ('Leaf', {1.0: 31}), 
                           ('Node', (0, 6.15), 
                                    ('Node', (1, 2.45), 
                                             ('Leaf', {2.0: 1}), ('Leaf', {1.0: 1})
                                    ), 
                                    ('Leaf', {2.0: 2})
                            )
                    ), 
                    ('Leaf', {2.0: 32})
         )
)
```

```mermaid
graph TD;
    A(x2 < 2.5)-->B(classe 0/33);
    A-->C;
    C(x3 < 1.7)-->D(x2 < 4.95);
    D-->E(classe 1/31);
    D-->F(x0 < 6.15);
    F-->G(x1<2.45);
    G-->H(classe 2/1);
    G-->I(classe 1/1);
    F-->J(classe 2/2);
    C-->K(classe 2/32);
```

Il apparaît que certains nœuds ne servent à discriminer que quelques cas.
Le problème relève ici du **sur-apprentissage**, car nous avons imposé que l'arbre cesse d'ajouter des nœuds uniquement lorsqu'une classe est sûre à 100 %.

Mais l'avantage des arbres de décision est qu'on peut les analyser, les comprendre, et éventuellement les modifier. On peut ici très facilement déduire de l'arbre précédent le suivant, plus simple : 

```mermaid
graph TD;
    A(x2 < 2.5)-->B(classe 0/33);
    A-->C;
    C(x3 < 1.7)-->D(classe 1/32 ou classe 2/3);
    C-->K(classe 2/32);
```

Ce qui correspond au code suivant, que vous pouvez injecter dans votre 
programme :

```python
tree2 = ('Node', (2, 2.5), 
                 ('Leaf', {0.0: 30}),
                 ('Node', (3, 1.7), 
                          ('Leaf', {1.0: 32, 2.0: 3}),
                          ('Leaf', {2.0: 32})
                )
         )
```

Les performance de ce dernier arbre qui a une meilleure faculté de généralisation seront assez souvent supérieures à l'arbre initialement appris.

## Avec `scikit-learn`

`scikit-learn` contient bien sûr le nécessaire pour faire des arbres de décision. Par défaut, c'est l'indice *Gini* qui est aussi utilisé, et l'arbre est construit jusqu'à des feuilles ne correspondant plus qu'à une seule classe. Mais il y a beaucoup de paramètres qui peuvent
être modifiés : [Doc DecisionTreeClassifier](https://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeClassifier.html)


```python
from sklearn.tree import DecisionTreeClassifier
clf = DecisionTreeClassifier()
clf.fit(train[:, :4], train[:, 4])
res = clf.predict(test[:, :4])
perf = np.count_nonzero(res == test[:, 4]) / test.shape[0] * 100
print(f"Performance avec scikit-learn : {perf} %")
```
<!--
from sklearn.tree import export_graphviz
export_graphviz(clf, out_file="tree.dot", feature_names=("x0", "x1", "x2", "x3"))
dot -Tpng tree.dot -o tree.png
-->

![](imgs/scikit_iris_decision_tree.png)

Plus généralement, on pourra consulter les pages de doc sur les arbres : [module tree](https://scikit-learn.org/stable/modules/tree.html)
