# Quelques algorithmes de machine learning

On propose dans ce dépôt d'explorer quelques algorithmes d'apprentissage automatique :

- [K-means](k-means.md)
- [k-plus proches voisins](kpp-voisins.md)
- [Arbre de décision](arbre_decision.md)
