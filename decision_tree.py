"""
Module decision_tree.

Les données sont représentées sous forme de tableau numpy, contenant autant
de lignes que d'échantillons. Chaque colonne correspond à une feature et la
dernière colonne, numérique aussi, est la classe. Dans la suite
ce type de donnée est appelé "tableau"

"""
from typing import List, Union
import numpy as np

# Type de données
tableau = np.array

def split(data: tableau, numvar: int, threshold: float) -> (tableau, tableau):
    """
    Sépare un tableau en deux parties selon une feature particulière
    et un seuil.

    :param data: les données d'entrée
    :param numvar: numéro de la feature pour séparer (0 est la première colonne)
    :param threshold: valeur du seuil
    :return (data1, data2): data1 contient les échantillons possédant la feature
                            numvar strictement inférieure à threshold
                            data2 dontient les autres échantillons
    """
    ...

def feuille_decision(data: tableau) -> dict:
    """
    Renvoie un dictionnaire qui comptabilise le nombre d'éléments
    de chaque classe

    :param data: tableau d'échantillons
    :return: dictionnaire contenant chaque classe en clé, et comme valeur
             associée le nombre d'éléments de cette classe dans  `data`
    """
    ...
    
def evalue_gini_set(data: tableau) -> float:
    """
    Renvoie l'indice de Gini d'un ensemble de données
    :param data: les données d'entrée
    :return: l'indice de Gini associé
    """
    ...

def evalue_gini(data1: tableau, data2: tableau) -> float:
    """
    Évalue l'indice de Gini des deux ensembles data1 et data2 selon
    la formule :
    nb1/N * gini(data1) + nb2/N * gini(data2)
    avec gini(data) = 1 - Σ (n_i/n)²

    :param data1: premier ensemble (fils gauche)
    :param data2: second ensemble (fils droit)
    :return: indice de Gini
    """
    ...

def choix_partitionne(data: tableau, debug=False) -> (int, float):
    """
    Pour un ensemble de données, sélectionne le meilleur critère de
    séparation en deux sous ensemble (en utilisant l'indice de Gini).

    :param data: matrice, échantillons en ligne et features en colonnes.
                La dernière colonne est le classement
    :param debug: affiche les infos de débuggage
    :returns: meilleure variable critère de partition et le seuil
    """
    ...

def construit_decision_tree(data: tableau, debug=False) -> tuple:
    """
    Construit l'arbre de décision représenté sous forme d'un tuple :
    ("Node", (num_var, threshold), (Sous arbre gauche), (Sous arbre droit))
    ou
    ("Leaf", dict {k: v})
    le dictionnaire indique le nombre de représentants `v` de chaque classe `k` présents
      sur la feuille
    Cette fonction est a priori récursive. Elle utilise `choix_partitionne`
    pour sélectionner la meilleure façon de partitionner.

    :param data: tableau des échantillons à classifier
    :param debug: pour prévoir les informations de débuggage
    :return: l'arbre de décision

    """
    ...

def classifie_(tree: tuple, ech: np.array):
    """
    Helper recursive function for classifie
    :param tree: arbre de décision tel que renvoyé par construit_decision_tree
    :param ech: l'échantillon à classifier
    :return: la feuille de l'arbre sur laquelle on atterit si on essaie de classer l'échantillon
    """
    ...

def classifie(tree: tuple, ech: Union[np.array, List]) -> (int, float):
    """
    Utilise un arbre de décision pour classer un nouvel échantillon.
    Cette fonction est à priori récursive.

    :param tree: arbre de décision tel que renvoyé par construit_decision_tree
    :param ech: l'échantillon à classifier
    :return: un tuple contenant la classe affecté et le degré de confiance (entre 0 et 1)
    """
    ...


if __name__ == '__main__':
    data =  np.array([[-1, 5, 0],[1, 3, 0],[3, 4, 0],[4, 1, 0],[6, 0, 0],
                      [4, 3, 1],[6, 2, 1],[8, 4, 1],[6, 6, 1],[4, 8, 1]])
    print(evalue_gini_set(data))            # 0.5
    print(evalue_gini(*split(data, 0, 5)))  # 0.4167
    print(choix_partitionne(data))          # 0, 3.5
    tree = construit_decision_tree(data)
    # ('Node', (0, 3.5), ('Leaf', {0: 3}), ('Node', (1, 1.5), ('Leaf', {0: 2}), ('Leaf', {1: 5})))
    print(tree)
    print(classifie(tree, [7, 1]))         # (0, 1.0) classe 0 à 100%
    print(classifie(tree, [5, 5]))         # (1, 1.0) classe 1 à 100%
