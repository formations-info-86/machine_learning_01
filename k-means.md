# Algorithms K-means

L'idée générale de l'algorithme K-means est très simple.
C'est un algorithme de création de clusters non supervisé.

1. On définit par avance un nombre de clusters (par exemple 3), puis on place le centre 
de ces clusters dans les données (points au hasard, ou échantillons pris au hasard).
2. Sur la base des centres des clusters, on calcule les clusters eux mêmes qui regroupent les échantillons
qui sont les plus proches du centre du cluster.
3. Une fois cette itération réalisée, on déplace les clusters au centre (barycentre) de tous les points du cluster.
4. On reprend à l'étape 2, jusqu'à ce que les centres des clusters soient stables.

Programmez cet algorithme sur les données iris, et voyez s'il est possible de discriminer les espèces d'Iris automatiquement
(note : le résultat n'est pas 100% satisfaisant... trouvez une explication !).
